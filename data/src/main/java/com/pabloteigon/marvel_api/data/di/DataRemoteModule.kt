package com.pabloteigon.marvel_api.data.di

import com.pabloteigon.marvel_api.data.BuildConfig
import com.pabloteigon.marvel_api.data.interceptor.CredentialsInterceptor
import com.pabloteigon.marvel_api.data.interceptor.HttpExceptionInterceptor
import com.pabloteigon.marvel_api.data.remote.api.ServerApi
import com.pabloteigon.marvel_api.data.remote.source.RemoteDataSource
import com.pabloteigon.marvel_api.data.remote.source.RemoteDataSourceImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val remoteDataSourceModule = module {
    factory {
        providesOkHttpClient()
    }

    single {
        createWebService<ServerApi>(okHttpClient = get(), url = BuildConfig.MARVEL_API_URL)
    }

    factory<RemoteDataSource> {
        RemoteDataSourceImpl(serverApi = get())
    }
}

fun providesOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    val exceptionInterceptor = HttpExceptionInterceptor()
    val credentialsInterceptor =
        CredentialsInterceptor(BuildConfig.MARVEL_PUBLIC_KEY, BuildConfig.MARVEL_PRIVATE_KEY)

    logging.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(logging)
        .addInterceptor(exceptionInterceptor)
        .addInterceptor(credentialsInterceptor)
        .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(url)
        .client(okHttpClient)
        .build()
        .create(T::class.java)
}