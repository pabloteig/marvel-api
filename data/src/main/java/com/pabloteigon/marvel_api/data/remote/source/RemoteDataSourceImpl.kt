package com.pabloteigon.marvel_api.data.remote.source

import com.pabloteigon.marvel_api.data.remote.api.ServerApi
import com.pabloteigon.marvel_api.data.remote.mapper.CharacterDataWrapperMapper
import com.pabloteigon.marvel_api.data.remote.mapper.ComicDataWrapperMapper
import com.pabloteigon.marvel_api.domain.entities.CharacterDataWrapper
import com.pabloteigon.marvel_api.domain.entities.ComicDataWrapper
import io.reactivex.Single

class RemoteDataSourceImpl(private val serverApi: ServerApi) : RemoteDataSource {

    override fun getListOfCharacters(offset: Int): Single<CharacterDataWrapper> {
        return serverApi.fetchListOfCharacters(offset)
            .map { CharacterDataWrapperMapper.map(it) }
    }

    override fun getListOfComicsByCharacterId(
        characterId: String,
        offset: Int
    ): Single<ComicDataWrapper> {
        return serverApi.fetchComicsByCharacterId(characterId, offset)
            .map { ComicDataWrapperMapper.map(it) }
    }
}