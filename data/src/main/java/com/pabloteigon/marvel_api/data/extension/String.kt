package com.pabloteigon.marvel_api.extension

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

fun String?.hashMD5(): String {
    return if (this.isNullOrEmpty()) {
        ""
    } else {
        try {
            val md5Encoder = MessageDigest.getInstance("MD5")
            val digest = md5Encoder.digest(this.toByteArray())
            return digest.fold("", { str, it -> str + "%02x".format(it) })
        } catch (e: NoSuchAlgorithmException) {
            throw Exception("cannot generate the api key", e)
        }
    }
}