package com.pabloteigon.marvel_api.data.di

import com.pabloteigon.marvel_api.data.repository.ComicsRepositoryImpl
import com.pabloteigon.marvel_api.data.repository.ListOfCharactersRepositoryImpl
import com.pabloteigon.marvel_api.domain.repository.ComicsRepository
import com.pabloteigon.marvel_api.domain.repository.ListOfCharactersRepository
import org.koin.dsl.module

val repositoryModule = module {

    factory<ListOfCharactersRepository> {
        ListOfCharactersRepositoryImpl(
            remoteDataSource = get()
        )
    }

    factory<ComicsRepository> {
        ComicsRepositoryImpl(
            remoteDataSource = get()
        )
    }

}

val dataModules = listOf(remoteDataSourceModule, repositoryModule)