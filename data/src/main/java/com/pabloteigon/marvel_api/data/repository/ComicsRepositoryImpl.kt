package com.pabloteigon.marvel_api.data.repository

import com.pabloteigon.marvel_api.data.remote.source.RemoteDataSource
import com.pabloteigon.marvel_api.domain.entities.ComicDataWrapper
import com.pabloteigon.marvel_api.domain.repository.ComicsRepository
import io.reactivex.Single

class ComicsRepositoryImpl(
    private val remoteDataSource: RemoteDataSource
) : ComicsRepository {

    override fun getListOfComicsByCharacterId(
        characterId: String,
        offset: Int
    ): Single<ComicDataWrapper> {
        return getListOfComicsByCharacterIdRemote(characterId, offset)
    }

    private fun getListOfComicsByCharacterIdRemote(
        characterId: String,
        offset: Int
    ): Single<ComicDataWrapper> {
        return remoteDataSource.getListOfComicsByCharacterId(characterId, offset)
            .flatMap { response ->
                Single.just(response)
            }
    }
}