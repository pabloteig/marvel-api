package com.pabloteigon.marvel_api.data.remote.model


data class ComicDataWrapperPayload(
    val code: Int,
    val status: String,
    val copyright: String,
    val attributionText: String,
    val attributionHTML: String,
    val data: ComicDataContainerPayload,
    val etag: String
)

data class ComicDataContainerPayload(
    val offset: Int,
    val limit: Int,
    val total: Int,
    val count: Int,
    val results: List<ComicPayload>
)

data class ComicPayload(
    val id: Int,
    val title: String,
    val description: String,
    val thumbnail: ThumbnailPayload
)