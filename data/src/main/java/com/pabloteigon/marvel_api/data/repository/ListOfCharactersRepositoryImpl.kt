package com.pabloteigon.marvel_api.data.repository

import com.pabloteigon.marvel_api.data.remote.source.RemoteDataSource
import com.pabloteigon.marvel_api.domain.entities.CharacterDataWrapper
import com.pabloteigon.marvel_api.domain.repository.ListOfCharactersRepository
import io.reactivex.Single

class ListOfCharactersRepositoryImpl(
    private val remoteDataSource: RemoteDataSource
) : ListOfCharactersRepository {

    override fun getListOfCharacters(
        offset: Int
    ): Single<CharacterDataWrapper> {
        return getListOfCharactersRemote(offset)
    }

    private fun getListOfCharactersRemote(
        offset: Int
    ): Single<CharacterDataWrapper> {
        return remoteDataSource.getListOfCharacters(offset)
            .flatMap { response ->
                Single.just(response)
            }
    }
}