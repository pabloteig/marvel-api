package com.pabloteigon.marvel_api.data.remote.model


data class CharacterDataWrapperPayload(
    val code: Int,
    val status: String,
    val copyright: String,
    val attributionText: String,
    val attributionHTML: String,
    val data: DataPayload,
    val etag: String
)

data class DataPayload(
    val offset: Int,
    val limit: Int,
    val total: Int,
    val count: Int,
    val results: List<ResultPayload>
)

data class ResultPayload(
    val id: Int,
    val name: String,
    val description: String,
    val modified: String,
    val thumbnail: ThumbnailPayload,
    val resourceURI: String,
    val comics: ComicsPayload,
    val series: Series,
    val stories: Stories,
    val events: Events,
    val urls: ArrayList<Url>
)

data class Events(
    val available: Int,
    val collectionURI: String,
    val items: List<ItemPayload>,
    val returned: Int
)

data class ItemPayload(
    val resourceURI: String,
    val name: String,
    val type: String
)

data class Url(
    val type: String,
    val url: String
)

data class ThumbnailPayload(
    val path: String,
    val extension: String
)

data class Stories(
    val available: Int,
    val collectionURI: String,
    val items: List<ItemPayload>,
    val returned: Int
)

data class ComicsPayload(
    val available: Int,
    val collectionURI: String,
    val items: List<ItemPayload>,
    val returned: Int
)

data class Series(
    val available: Int,
    val collectionURI: String,
    val items: List<ItemPayload>,
    val returned: Int
)