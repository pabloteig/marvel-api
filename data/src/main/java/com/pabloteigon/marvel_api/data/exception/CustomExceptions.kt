package com.pabloteigon.marvel_api.data.exception

class UnauthorizedException(message: String? = "") : Exception(message)

class AccessDeniedException(message: String? = "") : Exception(message)

class ConflictException(message: String? = "") : Exception(message)