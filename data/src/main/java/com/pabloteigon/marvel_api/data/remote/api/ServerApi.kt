package com.pabloteigon.marvel_api.data.remote.api

import com.pabloteigon.marvel_api.data.remote.model.CharacterDataWrapperPayload
import com.pabloteigon.marvel_api.data.remote.model.ComicDataWrapperPayload
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ServerApi {

    @GET("/v1/public/characters")
    fun fetchListOfCharacters(
        @Query("offset") offset: Int
    ): Single<CharacterDataWrapperPayload>

    @GET(" /v1/public/characters/{characterId}/comics")
    fun fetchComicsByCharacterId(
        @Path("characterId") characterId: String,
        @Query("offset") offset: Int
    ): Single<ComicDataWrapperPayload>

}