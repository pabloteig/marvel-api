package com.pabloteigon.marvel_api.data.interceptor

import com.pabloteigon.marvel_api.data.exception.AccessDeniedException
import com.pabloteigon.marvel_api.data.exception.ConflictException
import com.pabloteigon.marvel_api.data.exception.UnauthorizedException
import okhttp3.Interceptor
import okhttp3.Response

class HttpExceptionInterceptor : Interceptor {

    companion object {
        const val TAG = "HttpExceptionInterceptor"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.code() == 401) {
            throw UnauthorizedException(response.message())
        }
        if (response.code() == 403) {
            throw AccessDeniedException(response.message())
        }
        if (response.code() == 409) {
            throw ConflictException()
        }

        return response
    }
}