package com.pabloteigon.marvel_api.data.remote.mapper

import com.pabloteigon.marvel_api.data.remote.model.ComicDataContainerPayload
import com.pabloteigon.marvel_api.data.remote.model.ComicDataWrapperPayload
import com.pabloteigon.marvel_api.data.remote.model.ComicPayload
import com.pabloteigon.marvel_api.data.remote.model.ThumbnailPayload
import com.pabloteigon.marvel_api.domain.entities.Comic
import com.pabloteigon.marvel_api.domain.entities.ComicDataContainer
import com.pabloteigon.marvel_api.domain.entities.ComicDataWrapper
import com.pabloteigon.marvel_api.domain.entities.Thumbnail

object ComicDataWrapperMapper {

    fun map(payload: ComicDataWrapperPayload): ComicDataWrapper = comicDataWrapper(payload)

    private fun comicDataWrapper(payload: ComicDataWrapperPayload) = ComicDataWrapper(
        code = payload.code,
        status = payload.status,
        copyright = payload.copyright,
        attributionText = payload.attributionText,
        attributionHTML = payload.attributionHTML,
        data = comicDataContainer(payload.data),
        etag = payload.etag
    )

    private fun comicDataContainer(payload: ComicDataContainerPayload) = ComicDataContainer(
        offset = payload.offset,
        limit = payload.limit,
        total = payload.total,
        count = payload.count,
        results = payload.results.map { result(it) }
    )

    private fun result(payload: ComicPayload) = Comic(
        id = payload.id,
        title = payload.title,
        description = payload.description,
        thumbnail = thumbnail(payload.thumbnail)
    )

    private fun thumbnail(payload: ThumbnailPayload) = Thumbnail(
        path = payload.path,
        extension = payload.extension
    )
}