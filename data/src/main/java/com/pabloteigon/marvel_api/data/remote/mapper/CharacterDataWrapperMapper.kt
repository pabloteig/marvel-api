package com.pabloteigon.marvel_api.data.remote.mapper

import com.pabloteigon.marvel_api.data.remote.model.*
import com.pabloteigon.marvel_api.domain.entities.*

object CharacterDataWrapperMapper {

    fun map(payload: CharacterDataWrapperPayload): CharacterDataWrapper =
        characterDataWrapper(payload)

    private fun characterDataWrapper(payload: CharacterDataWrapperPayload) = CharacterDataWrapper(
        code = payload.code,
        status = payload.status,
        copyright = payload.copyright,
        attributionText = payload.attributionText,
        attributionHTML = payload.attributionHTML,
        data = data(payload.data),
        etag = payload.etag
    )

    private fun data(payload: DataPayload) = Data(
        offset = payload.offset,
        limit = payload.limit,
        total = payload.total,
        count = payload.count,
        results = payload.results.map { result(it) }
    )

    private fun result(payload: ResultPayload) = Result(
        id = payload.id,
        name = payload.name,
        description = payload.description,
        //modified = payload.modified,
        thumbnail = thumbnail(payload.thumbnail)
        //resourceURI = payload.resourceURI,
        //comics = comics(payload.comics)
    )

    private fun thumbnail(payload: ThumbnailPayload) = Thumbnail(
        path = payload.path,
        extension = payload.extension
    )

    private fun comics(payload: ComicsPayload) = Comics(
        available = payload.available,
        collectionURI = payload.collectionURI,
        items = payload.items.map { item(it) },
        returned = payload.returned
    )

    private fun item(payload: ItemPayload) = Item(
        resourceURI = payload.resourceURI,
        name = payload.name,
        type = payload.type
    )
}