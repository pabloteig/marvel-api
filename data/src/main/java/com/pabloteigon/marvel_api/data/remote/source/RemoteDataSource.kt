package com.pabloteigon.marvel_api.data.remote.source

import com.pabloteigon.marvel_api.domain.entities.CharacterDataWrapper
import com.pabloteigon.marvel_api.domain.entities.ComicDataWrapper
import io.reactivex.Single

interface RemoteDataSource {

    fun getListOfCharacters(
        offset: Int
    ): Single<CharacterDataWrapper>

    fun getListOfComicsByCharacterId(
        characterId: String,
        offset: Int
    ): Single<ComicDataWrapper>
}