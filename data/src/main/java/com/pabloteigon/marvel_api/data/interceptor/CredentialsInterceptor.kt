package com.pabloteigon.marvel_api.data.interceptor

import com.pabloteigon.marvel_api.extension.hashMD5
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class CredentialsInterceptor(
    private val publicKey: String,
    private val privateKey: String
) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val ts = System.currentTimeMillis().toString()
        val auth = ts + privateKey + publicKey
        var hash: String? = null

        try {
            hash = auth.hashMD5()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var request = chain.request()
        val url = request.url()
            .newBuilder()
            .addQueryParameter("ts", ts)
            .addQueryParameter("apikey", publicKey)
            .addQueryParameter("hash", hash)
            .build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}