package com.pabloteigon.marvel_api

import android.app.Application
import com.pabloteigon.marvel_api.data.di.dataModules
import com.pabloteigon.marvel_api.di.presentationModule
import com.pabloteigon.marvel_api.domain.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MarvelapiAplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MarvelapiAplication)

            modules(domainModule + dataModules + listOf(presentationModule))
        }
    }
}