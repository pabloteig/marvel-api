package com.pabloteigon.marvel_api.feature.characters.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pabloteigon.marvel_api.R
import com.pabloteigon.marvel_api.databinding.MainFragmentBinding
import com.pabloteigon.marvel_api.feature.AbstractFragment
import com.pabloteigon.marvel_api.utils.PaginationScrollListener
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : AbstractFragment() {

    override val viewModel: CharactersViewModel by viewModel()
    private val charactersAdapter: CharactersAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this.lifecycle.addObserver(viewModel)

        val binding = DataBindingUtil.inflate<MainFragmentBinding>(
            inflater, R.layout.main_fragment, container, false
        ).apply {
            vm = viewModel
            lifecycleOwner = this@MainFragment
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupErrorState(clCharactersFragmentContainer)

        rvCharactersList.apply {
            this.adapter = charactersAdapter
            postponeEnterTransition()
            viewTreeObserver
                .addOnPreDrawListener {
                    startPostponedEnterTransition()
                    true
                }

            addOnScrollListener(object : PaginationScrollListener() {
                override fun getTotalPageCount(): Int {
                    return 20
                }

                override fun loadMoreItems() {
                    viewModel.pagination()
                }

                override fun canRun(): Boolean {
                    return !viewModel.loadingVisibility.value!! && viewModel.hasData
                }
            })
        }
    }
}
