package com.pabloteigon.marvel_api.feature.characters.details

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.pabloteigon.marvel_api.domain.entities.Comic
import com.pabloteigon.marvel_api.domain.usecases.GetListOfComicsByCharacterIdUseCase
import com.pabloteigon.marvel_api.feature.viewmodel.BaseViewModel
import com.pabloteigon.marvel_api.feature.viewmodel.StateMachineSingle
import com.pabloteigon.marvel_api.feature.viewmodel.ViewState
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign

class DetailsViewModel(
    val getListOfComicsByCharacterIdUseCase: GetListOfComicsByCharacterIdUseCase,
    val uiScheduler: Scheduler
) : BaseViewModel(), LifecycleObserver {

    var characterId = ""
    val characterName = MutableLiveData<String>().apply { value = "" }
    val characterDescription = MutableLiveData<String>().apply { value = "" }
    val characterThumbnail = MutableLiveData<String>().apply { value = "" }

    val comics = MutableLiveData<List<Comic>>().apply { value = emptyList() }
    private val listOfComics = ArrayList<Comic>()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        getComicsByCharacterId()
    }

    private fun getComicsByCharacterId() {
        loadingVisibility.value = true
        disposables += getListOfComicsByCharacterIdUseCase.execute(
            characterId = characterId,
            offset = offset
        )
            .compose(StateMachineSingle())
            .observeOn(uiScheduler)
            .subscribe(
                {
                    if (it is ViewState.Success) {
                        addComics(it.data.data?.results!!)

                    } else if (it is ViewState.Failed) {
                        errorState.value = ViewState.Failed(it.throwable)
                    }
                    loadingVisibility.value = false
                },
                {
                    loadingVisibility.value = false
                    errorState.value = ViewState.Failed(it)
                }
            )
    }

    private fun addComics(listOfComics: List<Comic>) {
        hasData = listOfComics.isNotEmpty()

        if (hasData) {
            this.listOfComics.addAll(listOfComics)
            comics.value = this.listOfComics
        }
    }

    fun characterArgs(
        characterId: String,
        characterName: String,
        characterDescription: String,
        characterThumbnail: String
    ) {
        this.characterId = characterId
        this.characterName.value = characterName
        this.characterDescription.value = characterDescription
        this.characterThumbnail.value = characterThumbnail
    }

    override fun pagination() {
        super.pagination()
        getComicsByCharacterId()
    }
}