package com.pabloteigon.marvel_api.feature.characters.feed

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.pabloteigon.marvel_api.R
import com.pabloteigon.marvel_api.databinding.AdapterCharactersItemBinding
import com.pabloteigon.marvel_api.domain.entities.Result
import com.squareup.picasso.Picasso

class CharactersAdapter : RecyclerView.Adapter<CharactersAdapter.ViewHolder>() {

    private var characters: List<Result> = emptyList()

    companion object {
        @JvmStatic
        @BindingAdapter("characters")
        fun RecyclerView.bindItems(items: List<Result>) {
            val adapter = adapter as CharactersAdapter
            adapter.update(items)
        }

        @JvmStatic
        @BindingAdapter("bind:imageUrl")
        fun loadImage(view: ImageView, imageUrl: String?) {
            Picasso.get().load(imageUrl).centerCrop().fit()
                .placeholder(R.drawable.progress_animation).into(view)
        }
    }

    fun update(items: List<Result>) {
        this.characters = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_characters_item, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (characters[position]).let { character ->
            with(holder) {
                itemView.tag = character
                bind(createOnClickListener(binding, character), character)
            }
        }
    }

    private fun createOnClickListener(
        binding: AdapterCharactersItemBinding,
        character: Result
    ): View.OnClickListener {
        return View.OnClickListener {
            val characterId = character.id.toString()

            val directions = MainFragmentDirections.viewCharacterDetails(
                characterId = characterId,
                characterName = character.name!!,
                characterDescription = character.description!!,
                characterThumbnail = character.getThumbnailUrl()
            )

            val extras = FragmentNavigatorExtras(
                binding.tvCharacterName to "name_${characterId}",
                binding.tvCharacterDescription to "duration_${characterId}",
                binding.ivCharacterImg to "thumbnail_${characterId}"
            )
            it.findNavController().navigate(directions, extras)
        }
    }

    class ViewHolder(val binding: AdapterCharactersItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, value: Result) {
            ViewCompat.setTransitionName(binding.tvCharacterName, "title_${value.id}")
            ViewCompat.setTransitionName(binding.tvCharacterDescription, "duration_${value.id}")
            ViewCompat.setTransitionName(binding.ivCharacterImg, "thumbnail_${value.id}")
            with(binding) {
                this.character = value
                executePendingBindings()
            }
            binding.root.setOnClickListener(listener)
        }
    }

    override fun getItemCount(): Int = characters.size
}