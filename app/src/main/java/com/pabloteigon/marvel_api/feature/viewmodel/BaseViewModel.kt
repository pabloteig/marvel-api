package com.pabloteigon.marvel_api.feature.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    val errorState = MutableLiveData<ViewState<Nothing>>().apply {}
    val loadingVisibility = MutableLiveData<Boolean>().apply { value = false }
    val disposables = CompositeDisposable()
    var offset = 0
    var hasData: Boolean = true

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    open fun pagination() {
        offset += 20
    }
}