package com.pabloteigon.marvel_api.feature.characters.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.pabloteigon.marvel_api.R
import com.pabloteigon.marvel_api.domain.entities.Comic

class ComicsAdapter : RecyclerView.Adapter<ComicsAdapter.ComicsViewHolder>() {

    private var comics: List<Comic> = emptyList()

    companion object {
        @JvmStatic
        @BindingAdapter("comics")
        fun RecyclerView.bindItems(items: List<Comic>) {
            val adapter = adapter as ComicsAdapter
            adapter.update(items)
        }
    }

    fun update(items: List<Comic>) {
        this.comics = items
        notifyDataSetChanged()
    }

    class ComicsViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any) {
            binding.setVariable(BR.comic, data)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ComicsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.adapter_comics_item,
                parent,
                false
            )

        return ComicsViewHolder(binding)
    }

    override fun getItemCount(): Int = comics.size

    override fun onBindViewHolder(holder: ComicsViewHolder, position: Int) {
        holder.bind(comics[position])
    }
}