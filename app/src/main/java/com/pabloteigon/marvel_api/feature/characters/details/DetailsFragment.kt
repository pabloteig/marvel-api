package com.pabloteigon.marvel_api.feature.characters.details

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.navArgs
import com.pabloteigon.marvel_api.databinding.DetailsFragmentBinding
import com.pabloteigon.marvel_api.feature.AbstractFragment
import com.pabloteigon.marvel_api.utils.PaginationScrollListener
import kotlinx.android.synthetic.main.details_fragment.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailsFragment : AbstractFragment() {

    override val viewModel: DetailsViewModel by viewModel()
    private lateinit var binding: DetailsFragmentBinding
    private val args: DetailsFragmentArgs by navArgs()
    private val comicsAdapter: ComicsAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.characterArgs(
            characterId = args.characterId,
            characterName = args.characterName,
            characterDescription = args.characterDescription,
            characterThumbnail = args.characterThumbnail
        )

        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this.lifecycle.addObserver(viewModel)

        binding = DetailsFragmentBinding.inflate(inflater).apply {
            this.vm = viewModel
            lifecycleOwner = this@DetailsFragment
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setTransitionName(binding.videoTitle, "name_${args.characterId}")
        ViewCompat.setTransitionName(binding.videoThumbnail, "duration_${args.characterId}")
        ViewCompat.setTransitionName(binding.videoThumbnail, "thumbnail_${args.characterId}")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupErrorState(clDetailFragmentContainer)

        rvComicsList.apply {
            this.adapter = comicsAdapter
            postponeEnterTransition()
            viewTreeObserver
                .addOnPreDrawListener {
                    startPostponedEnterTransition()
                    true
                }

            addOnScrollListener(object : PaginationScrollListener() {
                override fun getTotalPageCount(): Int {
                    return 20
                }

                override fun loadMoreItems() {
                    viewModel.pagination()
                }

                override fun canRun(): Boolean {
                    return !viewModel.loadingVisibility.value!! && viewModel.hasData
                }
            })
        }
    }
}
