package com.pabloteigon.marvel_api.feature.characters.feed

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.pabloteigon.marvel_api.domain.entities.Result
import com.pabloteigon.marvel_api.domain.usecases.GetListOfCharactersUseCase
import com.pabloteigon.marvel_api.feature.viewmodel.BaseViewModel
import com.pabloteigon.marvel_api.feature.viewmodel.StateMachineSingle
import com.pabloteigon.marvel_api.feature.viewmodel.ViewState
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign

class CharactersViewModel(
    val getListOfCharactersUseCase: GetListOfCharactersUseCase,
    val uiScheduler: Scheduler
) : BaseViewModel(), LifecycleObserver {

    val characters = MutableLiveData<List<Result>>().apply { value = emptyList() }
    private val listOfCharacters = ArrayList<Result>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onStart() {
        getCharacters()
    }

    private fun getCharacters() {
        loadingVisibility.value = true
        disposables += getListOfCharactersUseCase.execute(offset = offset)
            .compose(StateMachineSingle())
            .observeOn(uiScheduler)
            .subscribe(
                {
                    if (it is ViewState.Success) {
                        addCharacters(it.data.data?.results!!)

                    } else if (it is ViewState.Failed) {
                        errorState.value = ViewState.Failed(it.throwable)
                    }
                    loadingVisibility.value = false
                },
                {
                    loadingVisibility.value = false
                    errorState.value = ViewState.Failed(it)
                }
            )
    }

    private fun addCharacters(listOfCharacters: List<Result>) {
        hasData = listOfCharacters.isNotEmpty()

        if (hasData) {
            this.listOfCharacters.addAll(listOfCharacters)
            characters.value = this.listOfCharacters
        }
    }

    override fun pagination() {
        super.pagination()
        getCharacters()
    }
}