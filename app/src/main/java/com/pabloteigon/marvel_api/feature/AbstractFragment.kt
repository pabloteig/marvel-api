package com.pabloteigon.marvel_api.feature

import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.pabloteigon.marvel_api.R
import com.pabloteigon.marvel_api.data.exception.ConflictException
import com.pabloteigon.marvel_api.data.exception.UnauthorizedException
import com.pabloteigon.marvel_api.extension.snack
import com.pabloteigon.marvel_api.feature.viewmodel.BaseViewModel
import com.pabloteigon.marvel_api.feature.viewmodel.ViewState
import java.net.UnknownHostException

abstract class AbstractFragment : Fragment() {

    companion object {
        const val TAG = "AbstractFragment"
    }

    abstract val viewModel: BaseViewModel

    protected fun setupErrorState(parentSnackView: View) {
        viewModel.errorState.observe(this, Observer { state ->
            when (state) {
                is ViewState.Failed -> {
                    Log.e(TAG, "Failed " + state.throwable)
                    when (state.throwable) {
                        is UnauthorizedException -> {
                            parentSnackView.snack(
                                message = getString(R.string.msg_no_internet_connection),
                                f = {})
                        }
                        is AccessDeniedException -> {
                            parentSnackView.snack(message = getString(R.string.comics), f = {})
                        }
                        is ConflictException -> {
                            parentSnackView.snack(message = state.throwable.message!!, f = {})
                        }
                        is UnknownHostException -> {
                            parentSnackView.snack(
                                message = getString(R.string.msg_no_internet_connection),
                                f = {})
                        }
                        else -> {
                            parentSnackView.snack(message = state.throwable.message ?: "", f = {})
                        }
                    }
                }
            }
        })
    }

}