package com.pabloteigon.marvel_api.di

import com.pabloteigon.marvel_api.feature.characters.details.ComicsAdapter
import com.pabloteigon.marvel_api.feature.characters.details.DetailsViewModel
import com.pabloteigon.marvel_api.feature.characters.feed.CharactersAdapter
import com.pabloteigon.marvel_api.feature.characters.feed.CharactersViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    factory { CharactersAdapter() }

    factory { ComicsAdapter() }

    viewModel {
        CharactersViewModel(
            getListOfCharactersUseCase = get(),
            uiScheduler = AndroidSchedulers.mainThread()
        )
    }

    viewModel {
        DetailsViewModel(
            getListOfComicsByCharacterIdUseCase = get(),
            uiScheduler = AndroidSchedulers.mainThread()
        )
    }
}