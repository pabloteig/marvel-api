package com.pabloteigon.marvel_api.di

import com.pabloteigon.marvel_api.data.di.dataModules
import com.pabloteigon.marvel_api.domain.di.domainModule

/**
 * Main Koin DI component.
 * Helps to configure
 * 1) Mockwebserver
 * 2) Usecase
 * 3) Repository
 */
fun configureTestAppComponent(baseApi: String) = listOf(
    MockWebServerDIPTest,
    configureNetworkModuleForTest(baseApi),
    domainModule,
    dataModules
)

