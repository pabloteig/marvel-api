package com.pabloteigon.marvel_api.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dev.ccodetest.base.BaseTest
import com.pabloteigon.marvel_api.data.BuildConfig
import com.pabloteigon.marvel_api.data.di.createWebService
import com.pabloteigon.marvel_api.data.di.providesOkHttpClient
import com.pabloteigon.marvel_api.data.remote.source.RemoteDataSourceImpl
import com.pabloteigon.marvel_api.data.repository.ComicsRepositoryImpl
import com.pabloteigon.marvel_api.di.configureTestAppComponent
import com.pabloteigon.marvel_api.domain.repository.ComicsRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin

@RunWith(JUnit4::class)
class ComicsRepositoryTest : BaseTest() {

    //Target
    private lateinit var mRepo: ComicsRepository

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val characterId = "1011334"
    private val mOffset = 0

    @Before
    fun start() {
        super.setUp()

        startKoin { listOf(configureTestAppComponent(getMockWebServerUrl())) }

    }

    @Test
    fun test_List_of_characters_repository_retrieves_expected_data() = runBlocking {

        mRepo = ComicsRepositoryImpl(
            remoteDataSource = RemoteDataSourceImpl(
                serverApi = createWebService(
                    okHttpClient = providesOkHttpClient(),
                    url = BuildConfig.MARVEL_API_URL
                )
            )
        )

        val dataReceived = mRepo.getListOfComicsByCharacterId(characterId, mOffset)

        assertNotNull(dataReceived)
    }
}