package com.pabloteigon.marvel_api.domain.entities


data class ComicDataWrapper(
    val code: Int?,
    val status: String?,
    val copyright: String?,
    val attributionText: String?,
    val attributionHTML: String?,
    val data: ComicDataContainer?,
    val etag: String?
)

data class ComicDataContainer(
    val offset: Int?,
    val limit: Int?,
    val total: Int?,
    val count: Int?,
    val results: List<Comic>?
)

data class Comic(
    val id: Int?,
    val title: String?,
    val description: String?,
    val thumbnail: Thumbnail?
) {
    fun getThumbnailUrl(): String {
        return thumbnail?.path + "." + thumbnail?.extension
    }
}