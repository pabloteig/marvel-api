package com.pabloteigon.marvel_api.domain.entities

data class CharacterDataWrapper(
    val code: Int?,
    val status: String?,
    val copyright: String?,
    val attributionText: String?,
    val attributionHTML: String?,
    val data: Data?,
    val etag: String?
)

data class Data(
    val offset: Int?,
    val limit: Int?,
    val total: Int?,
    val count: Int?,
    val results: List<Result>?
)

data class Result(
    val id: Int?,
    val name: String?,
    val description: String?,
    //val modified: String?,
    val thumbnail: Thumbnail?
    //val resourceURI: String?,
    //val comics: Comics,
    //val series: Series? = null,
    //val stories: Stories? = null,
    //val events: Events? = null,
    //val urls: ArrayList<Url>? = null
){
    fun getThumbnailUrl(): String {
        return thumbnail?.path + "." + thumbnail?.extension
    }
}

data class Events(
    val available: Int,
    val collectionURI: String,
    val items: List<Item>,
    val returned: Int
)

data class Item(
    val resourceURI: String?,
    val name: String?,
    val type: String?
)

data class Url(
    val type: String,
    val url: String
)

data class Thumbnail(
    val path: String?,
    val extension: String?
)

data class Stories(
    val available: Int,
    val collectionURI: String,
    val items: List<Item>,
    val returned: Int
)

data class Comics(
    val available: Int,
    val collectionURI: String,
    val items: List<Item>,
    val returned: Int
)

data class Series(
    val available: Int,
    val collectionURI: String,
    val items: List<Item>,
    val returned: Int
)