package com.pabloteigon.marvel_api.domain.usecases

import com.pabloteigon.marvel_api.domain.entities.CharacterDataWrapper
import com.pabloteigon.marvel_api.domain.repository.ListOfCharactersRepository
import io.reactivex.Scheduler
import io.reactivex.Single

interface GetListOfCharactersUseCase {
    fun execute(offset: Int): Single<CharacterDataWrapper>
}

class GetListOfCharactersUseCaseImpl(
    private val listOfCharactersRepository: ListOfCharactersRepository,
    private val scheduler: Scheduler
) : GetListOfCharactersUseCase {

    override fun execute(offset: Int): Single<CharacterDataWrapper> {
        return listOfCharactersRepository.getListOfCharacters(offset)
            .subscribeOn(scheduler)
    }
}