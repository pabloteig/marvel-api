package com.pabloteigon.marvel_api.domain.repository

import com.pabloteigon.marvel_api.domain.entities.CharacterDataWrapper
import io.reactivex.Single

interface ListOfCharactersRepository {

    fun getListOfCharacters(offset: Int): Single<CharacterDataWrapper>
}