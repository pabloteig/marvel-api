package com.pabloteigon.marvel_api.domain.usecases

import com.pabloteigon.marvel_api.domain.entities.ComicDataWrapper
import com.pabloteigon.marvel_api.domain.repository.ComicsRepository
import io.reactivex.Scheduler
import io.reactivex.Single

interface GetListOfComicsByCharacterIdUseCase {
    fun execute(characterId: String, offset: Int): Single<ComicDataWrapper>
}

class GetListOfComicsByCharacterIdUseCaseImpl(
    private val comicsRepository: ComicsRepository,
    private val scheduler: Scheduler
) : GetListOfComicsByCharacterIdUseCase {

    override fun execute(
        characterId: String,
        offset: Int
    ): Single<ComicDataWrapper> {
        return comicsRepository.getListOfComicsByCharacterId(characterId, offset)
            .subscribeOn(scheduler)
    }
}