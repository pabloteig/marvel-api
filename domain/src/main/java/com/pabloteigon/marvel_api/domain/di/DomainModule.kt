package com.pabloteigon.marvel_api.domain.di

import com.pabloteigon.marvel_api.domain.usecases.*
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.module

val useCaseModule = module {

    factory<GetListOfCharactersUseCase> {
        GetListOfCharactersUseCaseImpl(
            listOfCharactersRepository = get(),
            scheduler = Schedulers.io()
        )
    }

    factory<GetListOfComicsByCharacterIdUseCase> {
        GetListOfComicsByCharacterIdUseCaseImpl(
            comicsRepository = get(),
            scheduler = Schedulers.io()
        )
    }
}

val domainModule = listOf(useCaseModule)