package com.pabloteigon.marvel_api.domain.repository

import com.pabloteigon.marvel_api.domain.entities.ComicDataWrapper
import io.reactivex.Single

interface ComicsRepository {

    fun getListOfComicsByCharacterId(
        characterId: String,
        offset: Int
    ): Single<ComicDataWrapper>
}